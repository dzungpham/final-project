''' Controller for /movies/{id}
    Imported from handler for /movies '''
import apiutil
import sys
import os.path
sys.stdout = sys.stderr # Turn off console output; it will get logged by Apache
import threading
import cherrypy
import os
import os.path
import math
import json
from collections import OrderedDict
import mysql.connector
from mysql.connector import Error
from jinja2 import Environment, FileSystemLoader
env = Environment(loader=FileSystemLoader(os.path.abspath(os.path.dirname(__file__))+'/templates/'))
import logging
from config import conf

class MovieID(object):
    ''' Handles resource /movies/{id} 
        Allowed methods: GET '''
    exposed = True

    def __init__(self):
        self.myd = dict()
        self.xtra = dict()
        self.db = dict()
        self.db['name']='MovieMagic'
        self.db['user']='root'
        self.db['host']='127.0.0.1'

    def getRecommendation(self,ID):
        self.myd = dict()
        self.xtra = dict()
        try:
            cnx = mysql.connector.connect(
                user=self.db['user'],
                host=self.db['host'],
                database=self.db['name'],
            )
            cursor = cnx.cursor()
            q="select DVDTitle,studio,status,price,rating,year,genre,DVDid from DVDList where genre=(select genre from DVDList where DVDid="+ID+") and studio=(select studio from DVDList where DVDid="+ID+");"
            cursor.execute(q)
        except Error as e:
            logging.error(e)
            raise

        for (DVDTitle, studio, status, price, rating, year, genre, DVDid) in cursor:
            print DVDTitle
            href='movies/'+str(ID)
            self.myd[DVDid] = DVDid
            self.xtra[DVDid]=(DVDTitle, studio, status, price, rating, year, genre, href)


    def GET(self, movieID):
        ''' Return information on movieID '''
        output_format = cherrypy.lib.cptools.accept(['text/html', 'application/json'])

        self.getRecommendation(movieID)
        if output_format == 'text/html':
            return env.get_template('recommendation-tmpl.html').render(
                movies=self.myd,
                info=self.xtra,
                base=cherrypy.request.base.rstrip('/') + '/'
            )
        else:
            return json.dumps(self.data, encoding='utf-8')
