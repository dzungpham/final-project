# Dzung Pham and Jillian Montalvo
# Final Project for Web Applications
# createDB.py: Create MovieMagic database and its tables

import mysql.connector

# Define database variables
DATABASE_USER = 'root'
DATABASE_HOST = '127.0.0.1'
DATABASE_NAME = 'MovieMagic'

# Create connection to mySQL
cnx = mysql.connector.connect(user=DATABASE_USER, host=DATABASE_HOST, use_unicode=True)
cursor = cnx.cursor()

# Create database if not exist
createDB = (("CREATE DATABASE IF NOT EXISTS %s DEFAULT CHARACTER SET utf8mb4") % (DATABASE_NAME))
cursor.execute(createDB)

# Switch to MusicMagic DB
useDB = (("USE %s") % (DATABASE_NAME))
cursor.execute(useDB)

#########################
# Drop all tables first
#########################

# directorsIndex
dropTableQuery = ("DROP TABLE IF EXISTS directorsIndex")
cursor.execute(dropTableQuery)

# directorsName
dropTableQuery = ("DROP TABLE IF EXISTS directorsName")
cursor.execute(dropTableQuery)

# actorIndex
dropTableQuery = ("DROP TABLE IF EXISTS actorIndex")
cursor.execute(dropTableQuery)

# actorNames
dropTableQuery = ("DROP TABLE IF EXISTS actorNames")
cursor.execute(dropTableQuery)

# DVDList
dropTableQuery = ("DROP TABLE IF EXISTS DVDList")
cursor.execute(dropTableQuery)

#########################
# Create tables
#########################
# dvdList
createTableQuery = ('''CREATE TABLE DVDList (
						DVDid INT NOT NULL,
						DVDTitle VARCHAR(100) CHARACTER SET utf8mb4 NOT NULL, 
						studio VARCHAR(100) NOT NULL, 
						status ENUM('Out', 'Discontinued', 'Pending', 'Cancelled'),
						price VARCHAR(50) NOT NULL,
						rating VARCHAR(20),
						year VARCHAR(20) NOT NULL,
						genre VARCHAR(50),
						PRIMARY KEY (DVDid)
						)'''
					)
cursor.execute(createTableQuery)

# actorNames
createTableQuery = ('''CREATE TABLE actorNames (
						actorID INT NOT NULL AUTO_INCREMENT,
						lastName VARCHAR(100) CHARACTER SET utf8mb4 NOT NULL, 
						firstName VARCHAR(100) CHARACTER SET utf8mb4, 
						PRIMARY KEY (actorID)
						)'''
					)
cursor.execute(createTableQuery)

# actorIdex
createTableQuery = ('''CREATE TABLE actorIndex (
						id INT NOT NULL AUTO_INCREMENT,
						actorID INT NOT NULL,
						DVDid INT NOT NULL, 
						PRIMARY KEY (id),
						FOREIGN KEY (actorID) REFERENCES actorNames(actorID) ON DELETE CASCADE
						)'''
					)
cursor.execute(createTableQuery)

# directorsName
createTableQuery = ('''CREATE TABLE directorsName (
						Director_id INT NOT NULL AUTO_INCREMENT, 
						lastName VARCHAR(100) CHARACTER SET utf8mb4 NOT NULL, 
						firstName VARCHAR(100) CHARACTER SET utf8mb4 NOT NULL, 
						PRIMARY KEY (Director_id)
						)'''
					)
cursor.execute(createTableQuery)

# directorsIndex
createTableQuery = ('''CREATE TABLE directorsIndex (
						ID INT NOT NULL AUTO_INCREMENT,
						Director_id INT NOT NULL,
						DVDList_id INT NOT NULL,
						PRIMARY KEY(id)
						# FOREIGN KEY (Director_id) REFERENCES directorsName(Director_id) ON DELETE CASCADE
						)'''
					)
cursor.execute(createTableQuery)

# Commit the data and close the connection to MySQL
cnx.commit()
cnx.close()