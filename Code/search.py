''' /search resource for ndcse.com
This is run as a WSGI application through CherryPy and Apache with mod_wsgi
Author: Jillian Montalvo
Date: April, 20 2015
Web Applications'''
import apiutil
from apiutil import errorJSON
import sys
import os.path
sys.stdout = sys.stderr # Turn off console output; it will get logged by Apache
import threading
import cherrypy
import os
import os.path
import json
import re
from collections import OrderedDict
import mysql.connector
from mysql.connector import Error
from jinja2 import Environment, FileSystemLoader
env = Environment(loader=FileSystemLoader(os.path.abspath(os.path.dirname(__file__))+'/templates/'))
import logging
from config import conf

class Search(object):
    ''' Handles resource /search
        Allowed methods: GET'''
    exposed = True

    def __init__(self):
        self.myd = dict()
        self.db = dict()
        self.db['name']='MovieMagic'
        self.db['user']='root'
        self.db['host']='127.0.0.1'

    def _cp_dispatch(self,vpath):
            print "Search._cp_dispatch with vpath: %s \n" % vpath
#            if len(vpath) == 1: # /search/{title}
#                cherrypy.request.params['title']=vpath.pop(0)
#                return self
    
            return vpath

    def getDataFromDB(self):
        try:
            cnx = mysql.connector.connect(
                user=self.db['user'],
                host=self.db['host'],
                database=self.db['name'],
            )
            cursor = cnx.cursor()
            q="select DVDTitle, studio, status, price, rating, year, genre, DVDid from DVDList;"
            cursor.execute(q)
        except Error as e:
            logging.error(e)
            raise
        for (DVDTitle, studio, status, price, rating, year, genre, DVDid) in cursor:
            self.myd[DVDid]={'DVDid':DVDid,
                         'name':DVDTitle,
                         'studio':studio,
                         'status':status,
                         'price':price,
                         'rating':rating,
                         'year':year,
                         'genre':genre
                         }

    @cherrypy.tools.json_in(force=False)
    def POST(self,title=None):
        ''' Display Movies with matching title '''
        if not title:
            try:
                title = cherrypy.request.json["title"]
                print "title received: %s" % title
            except:
                print "Title not received"
                return errorJSON(code=9003, message="Expected text 'title' for user JSON input")

        # Search for titles containing search criteria 'title'
        self.getDataFromDB()
        matches = list()
        for DVDid in self.myd:
            search = str(title).lower()
            DVD = str(self.myd[DVDid]['name']).lower()
            if search in DVD:
                matches.append(self.myd[DVDid])

        result={"DVDs": matches, "errors":[]}
        return json.dumps(result)

class StaticAssets(object):
    pass

if __name__ == '__main__':
    conf = {
        'global': {
            'request.dispatch': cherrypy.dispatch.MethodDispatcher(),
            'tools.staticdir.root': os.path.dirname(os.path.abspath(__file__))
        },
        '/css': {
            'tools.staticdir.on': True,
            'tools.staticdir.dir': 'css'
        },
        '/js': {
            'tools.staticdir.on': True,
            'tools.staticdir.dir': 'js'
        }
    }
    cherrypy.tree.mount(Search(), '/search', {
        '/': {
            'request.dispatch': cherrypy.dispatch.MethodDispatcher()
        }
    })
    cherrypy.tree.mount(StaticAssets(), '/', {
        '/': {
            'tools.staticdir.root': os.path.dirname(os.path.abspath(__file__))
        },
        '/css': {
            'tools.staticdir.on': True,
            'tools.staticdir.dir': 'css'
        },
        '/js': {
            'tools.staticdir.on': True,
            'tools.staticdir.dir': 'js'
        }
    })
    cherrypy.engine.start()
    cherrypy.engine.block()
else:
    application = cherrypy.Application(Search(), None, conf)

