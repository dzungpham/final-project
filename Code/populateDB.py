#!/usr/bin/python

# Dzung Pham and Jillian Montalvo
# Final Project for Web Applications
# populateDB.py: Populate database with json formatted data

import mysql.connector
from mysql.connector import Error
import logging
import sys
import random
import re
import cjson
import subprocess

logging.basicConfig(filename='mysql.log', level=logging.DEBUG, format='%s(asctime)s %(message)s')

# Define database variables
DATABASE_USER = 'root'
DATABASE_HOST = '127.0.0.1'
DATABASE_NAME = 'MovieMagic'

# Create connection to mySQL
cnx = mysql.connector.connect(user=DATABASE_USER, host=DATABASE_HOST, database=DATABASE_NAME,
							charset='utf8mb4', use_unicode=True)
cursor = cnx.cursor()

##########################
# DVDList table
##########################
DVDList = cjson.decode(open('Data/DVDList.json', 'r').read())

for d in DVDList:
	item = {
		'DVDid': d['ID'],
		'DVDTitle': d['DVD_Title'],
		'studio': d['Studio'],
		'status': d['Status'],
		'price': d['Price'],
		'rating': d['Rating'],
		'year': d['Year'],
		'genre': d['Genre']
	}
	# Insert into database
	addDVDList = ("INSERT INTO DVDList (DVDid, DVDTitle, studio, status, price, rating, year, genre) VALUES (%(DVDid)s, %(DVDTitle)s, %(studio)s, %(status)s, %(price)s, %(rating)s, %(year)s,%(genre)s)")
	cursor.execute(addDVDList,item)

##########################
# actorNames table
##########################
actorNameList = cjson.decode(open('Data/actorNames.json', 'r').read())	

for d in actorNameList:
	if (len(d['Actor'].split(',')) < 2):
		firstName = "NULL"
	else:
		firstName = d['Actor'].split(',')[1]
	item = {
		'actorID': d['Actor_id'],
		'lastName': d['Actor'].split(',')[0],
		'firstName': firstName
	}
	# Insert into database
	addActorName = ("INSERT INTO actorNames (actorID, lastName, firstName) VALUES (%(actorID)s, %(lastName)s, %(firstName)s)")
	cursor.execute(addActorName,item)

##########################
# actorIndex table
##########################
actorNameList = cjson.decode(open('Data/actorIndex.json', 'r').read())	

for d in actorNameList:
	item = {
		'id': d['ID'],
		'actorID': d['Actor_id'],
		'DVDid': d['DVDList_id']
	}
	# Insert into database
	addActorIndex = ("INSERT INTO actorIndex (id, actorID, DVDid) VALUES (%(id)s, %(actorID)s, %(DVDid)s)")
	cursor.execute(addActorIndex,item)

##########################
# directorsName table
##########################
subprocess.call("./DirectorName.sh", shell=True)

##########################
# directorsIndex table
##########################
directorIndexList = cjson.decode(open('Data/directorsIndex.json', 'r').read())	

for d in directorIndexList:
	item = {
		'ID': d['ID'],
		'Director_id': d['Director_id'],
		'DVDList_id': d['DVDList_id']
	}
	# Insert into database
	addDirectorIndex = ("INSERT INTO directorsIndex (ID, Director_id, DVDList_id) VALUES (%(ID)s, %(Director_id)s, %(DVDList_id)s)")
	cursor.execute(addDirectorIndex,item)

# Commit and close
cnx.commit()
cnx.close()