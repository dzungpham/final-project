''' /movies resource for ndcse.com
This is run as a WSGI application through CherryPy and Apache with mod_wsgi
Author: Jillian Montalvo
Date: April, 20 2015
Web Applications'''
import apiutil
import sys
import os.path
sys.stdout = sys.stderr # Turn off console output; it will get logged by Apache
import threading
import cherrypy
import os
import os.path
import math
import json
from collections import OrderedDict
from movieid import MovieID
import mysql.connector
from mysql.connector import Error
from jinja2 import Environment, FileSystemLoader
env = Environment(loader=FileSystemLoader(os.path.abspath(os.path.dirname(__file__))+'/templates/'))
import logging
from config import conf

class Movies(object):
    ''' Handles resource /movies
        Allowed methods: GET'''
    exposed = True

    def __init__(self):
        self.id=MovieID()
        self.myd = dict()
        self.xtra = dict()
        self.db = dict()
        self.db['name']='MovieMagic'
        self.db['user']='root'
        self.db['host']='127.0.0.1'

    def _cp_dispatch(self,vpath):
            print "Movies._cp_dispatch with vpath: %s \n" % vpath
            if len(vpath) == 1: # /movies/{id}
                cherrypy.request.params['movieID']=vpath.pop(0)
                return self.id
    
            return vpath

    def GET(self):
        ''' Display Search Bar '''
        output_format = cherrypy.lib.cptools.accept(['text/html', 'application/json'])

        if output_format == 'text/html':
            return env.get_template('movies-tmpl.html').render(
                base=cherrypy.request.base.rstrip('/') + '/'
            )
        else:
            return json.dumps(self.data, encoding='utf-8')

class StaticAssets(object):
    pass

if __name__ == '__main__':
    conf = {
        'global': {
            'request.dispatch': cherrypy.dispatch.MethodDispatcher(),
            'tools.staticdir.root': os.path.dirname(os.path.abspath(__file__))
        },
        '/css': {
            'tools.staticdir.on': True,
            'tools.staticdir.dir': 'css'
        },
        '/js': {
            'tools.staticdir.on': True,
            'tools.staticdir.dir': 'js'
        }
    }
    cherrypy.tree.mount(Movies(), '/movies', {
        '/': {
            'request.dispatch': cherrypy.dispatch.MethodDispatcher()
        }
    })
    cherrypy.tree.mount(StaticAssets(), '/', {
        '/': {
            'tools.staticdir.root': os.path.dirname(os.path.abspath(__file__))
        },
        '/css': {
            'tools.staticdir.on': True,
            'tools.staticdir.dir': 'css'
        },
        '/js': {
            'tools.staticdir.on': True,
            'tools.staticdir.dir': 'js'
        }
    })
    cherrypy.engine.start()
    cherrypy.engine.block()
else:
    application = cherrypy.Application(Movies(), None, conf)

