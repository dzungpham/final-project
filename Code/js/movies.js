/* Wait until the DOM loads by wrapping our code in a callback to $. */
$(function() {
	var showAlert = function(message, type) {

            /* This stuctured mess of code creates a Bootstrap-style alert box.
             * Note the use of chainable jQuery methods. */
            var $alert = (
                          $('<div>')                // create a <div> element
                          .text(message)          // set its text
                          .addClass('alert')      // add some CSS classes and attributes
                          .addClass('alert-' + type)
                          .addClass('alert-dismissible')
                          .attr('role', 'alert')
                          .prepend(               // prepend a close button to the inner HTML
                                   $('<button>')         // create a <button> element
                                   .attr('type', 'button') // and so on...
                                   .addClass('close')
                                   .attr('data-dismiss', 'alert')
                                   .html('&times;')    // &times; is code for the x-symbol
                                                  )
                          .hide()  // initially hide the alert so it will slide into view
                          );

            /* Add the alert to the alert container. */
            $('#alerts').append($alert);

            /* Slide the alert into view with an animation. */
            $alert.slideDown();
        };

	$('#search-form').on('submit', function(event) { // form id
	
		var title = $('#user-title-input').val(); // input id
		var results = document.getElementById("movie-list");
		
		event.preventDefault();

		$.ajax({
			/* The HTTP method. */
			type: 'POST',
			/* The URL. Use a dummy order ID for now. */
			url: '/search/',
			/* The `Content-Type` header. This tells the server what format the body
			 * of our request is in. This sets the header as
			 * `Content-Type: application/json`. */
			contentType: 'application/json',
			/* The request body. `JSON.stringify` formats it as JSON. */
			data: JSON.stringify({
				title: title
			}),
			dataType: 'json'
			
			}).done(function(data){
				var error = data['errors'];
				if (error.length < 1){
				    var result = data['DVDs'];
				    results.innerHTML = "";
				    if (result.length == 0) results.appendChild(document.createTextNode("NO RESULTS FOUND"));
				    for(var i = 0, n = result.length; i < n; ++i) {
					var movie = result[i];
					var li = document.createElement("LI");
					var a = document.createElement("A"); 
					a.href = '/movies/' + movie['DVDid'];
					var t = document.createTextNode(movie['name']); 
					a.appendChild(t); 
					li.appendChild(a);
					results.appendChild(li);
				    }
				}else{
				    showAlert(JSON.stringify(error[0]['message']),'danger');
				}
			}).fail(function() {
				showAlert('One or More Errors on Page.', 'danger');
		});
	    });

});
