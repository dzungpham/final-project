/* Wait until the DOM loads by wrapping our code in a callback to $. */
$(function() {

	var $orderId = $('#order-id');

	/* A function to show an alert box at the top of the page. */
	var showAlert = function(message, type) {

	    /* This stuctured mess of code creates a Bootstrap-style alert box.
	     * Note the use of chainable jQuery methods. */
	    var $alert = (
			  $('<div>')                // create a <div> element
			  .text(message)          // set its text
			  .addClass('alert')      // add some CSS classes and attributes
			  .addClass('alert-' + type)
			  .addClass('alert-dismissible')
			  .attr('role', 'alert')
			  .prepend(               // prepend a close button to the inner HTML
				   $('<button>')         // create a <button> element
				   .attr('type', 'button') // and so on...
				   .addClass('close')
				   .attr('data-dismiss', 'alert')
				   .html('&times;')    // &times; is code for the x-symbol
						  )
			  .hide()  // initially hide the alert so it will slide into view
			  );

	    /* Add the alert to the alert container. */
	    $('#alerts').append($alert);

	    /* Slide the alert into view with an animation. */
	    $alert.slideDown();
	};



  /* Add click event listeners to the restaurant list items. This adds a
   * handler for each element matching the CSS selector
   * .restaurant-list-item. */
  $('.order-list-item a').click(function(event) {

    /* Prevent the default link navigation behavior. */
    event.preventDefault();

    var $order = $(this);
    var $categories = $order.parents('.order-list-item').find('.category-sublist');

    /* If the category list is shown, hide it. */
    if($categories.is(':visible')) {
      $categories.slideUp();
      return;
    }

    /* Fade out all other category lists. */
    $('.category-sublist').not($categories).slideUp();

    /* Get the category JSON data via Ajax. */
    $.ajax({
      type: 'GET',
      url: $order.attr('href'),
      dataType: 'json'
    }).done(function(data) {

      /* This gets called if the Ajax call is successful. */

      /* We expect the JSON data to be in this form:
       *   [
       *     {
       *       "href": <url-of-category>,
       *       "name": <name-of-category>
       *     },
       *     ...
       *   ]
       */

      /* Empty out existing contents in the category list. */
      $categories.empty();

      /* Add a list item/link for each category received. */
      var categories = data;
      for(var i = 0, n = categories.length; i < n; ++i) {
        var category = categories[i];
        $categories.append(
          $('<a>')
            .addClass('list-group-item no-gutter')
            .attr('href', category.href)
            .append(
              $('<div>')
                .text(category.name)
                .addClass('list-group-item-heading no-gutter')
            )
        );
      }
      /* Slide the newly populated category list into view. */
      $categories.slideDown();
    }).fail(function() {

      /* This gets called if the Ajax call fails. */

      $categories.empty().slideUp();

      /* Create an alert box. */
      var $alert = (
        $('<div>')
          .text('Whoops! Something went wrong.')
          .addClass('alert')
          .addClass('alert-danger')
          .addClass('alert-dismissible')
          .attr('role', 'alert')
          .prepend(
            $('<button>')
              .attr('type', 'button')
              .addClass('close')
              .attr('data-dismiss', 'alert')
              .html('&times;')
          )
          .hide()
      );
      /* Add the alert to the alert container. */
      $('#alerts').append($alert);
      /* Slide the alert into view with an animation. */
      $alert.slideDown();
    });
  });

  /*Add click event for delete button */
  $('#create-order-btn').click(function() {
	  event.preventDefault();
	  $.ajax({
		  type: 'POST',
		      url: '/orders/',
		      contentType: 'application/json',
		      dataType: 'json'
		      }).done(function() {
			      /* Show a success message. */
			      showAlert('Created the Order!', 'success');
			      window.location = window.location.href;
			  }).fail(function() {
				  showAlert('Something went wrong.', 'danger');
			      });
      });

  $('#delete-order-btn').click(function() {
	  event.preventDefault();
	  $.ajax({
		  type: 'PUT',
		      url: '/orders/'+ $orderId.val(),
		      contentType: 'application/json',
		      dataType: 'json'
		      }).done(function() {
			      /* Show a success message. */
			      showAlert('Deleted the Order!', 'success');
			      window.location = window.location.href;
			  }).fail(function() {
				  showAlert('Something went wrong.', 'danger');
			      });
      });
});
